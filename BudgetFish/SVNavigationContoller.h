//
//  SVNavigationContoller.h
//  BudgetFish
//
//  Created by Steve Viselli on 21/09/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model/SVBudget.h"

@interface SVNavigationContoller : UINavigationController <UINavigationControllerDelegate>

@property (nonatomic, strong) SVBudget *currentBudget;
@property (nonatomic, readonly) float budgetUsed;

- (void)updateBudget;
- (void)updateWaterLevel;
- (void)deleteAllExpenses;
- (void)fadeOutFish;
- (void)fadeInFish;

@end
