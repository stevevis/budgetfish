//
//  SVPushAnimator.h
//  BudgetFish
//
//  Created by Steve Viselli on 23/09/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SlideLeft,
    SlideRight
} SlideDirection;

@interface SVSlideNavigationAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) SlideDirection slideDirection;

@end
