//
//  SVExpenseListController.h
//  BudgetFish
//
//  Created by Steve Viselli on 3/11/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVExpenseListController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *expenseTableView;
@property (weak, nonatomic) IBOutlet UIView *bottomNavView;

- (IBAction)back:(id)sender;

@end
