//
//  SVCoreDataStack.h
//  BudgetFish
//
//  Created by Steve Viselli on 14/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVCoreDataStack : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype) defaultStack;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
