//
//  SVPushAnimator.m
//  BudgetFish
//
//  Created by Steve Viselli on 23/09/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVSlideNavigationAnimator.h"

@implementation SVSlideNavigationAnimator

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    UIView *toView = toViewController.view;
    UIView *fromView = fromViewController.view;
    
    if (self.slideDirection == SlideLeft) {
        toView.center = CGPointMake(fromView.center.x + fromView.frame.size.width, fromView.center.y);
    }
    else if (self.slideDirection == SlideRight) {
        toView.center = CGPointMake(fromView.center.x - fromView.frame.size.width, fromView.center.y);
    }
    
    [[transitionContext containerView] addSubview:toViewController.view];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        toView.center = CGPointMake(fromView.center.x, toView.center.y);
        
        if (self.slideDirection == SlideLeft) {
            fromView.center = CGPointMake(0 - fromView.center.x, fromView.center.y);
        }
        else if (self.slideDirection == SlideRight) {
            fromView.center = CGPointMake(fromView.center.x + fromView.frame.size.width, fromView.center.y);
        }
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}

@end
