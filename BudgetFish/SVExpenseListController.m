//
//  SVExpenseListController.m
//  BudgetFish
//
//  Created by Steve Viselli on 3/11/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVExpenseListController.h"
#import "SVCoreDataStack.h"
#import "Model/SVCategory.h"
#import "Model/SVExpense.h"
#import "SVNavigationContoller.h"

static NSDateFormatter *dateFormatter = nil;

@interface SVExpenseListController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation SVExpenseListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Make sure everything is clear
    UIColor *clearColor = [UIColor clearColor];
    [self.view setBackgroundColor:clearColor];
    [self.bottomNavView setBackgroundColor:clearColor];
    
    // Make the table view clear
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectZero];
    [clearView setBackgroundColor:clearColor];
    [self.expenseTableView setBackgroundView:clearView];
    [self.expenseTableView setBackgroundColor:clearColor];
    
    // Don't need a navigation back button
    self.navigationItem.hidesBackButton = true;
    
    // Set the table view delegate and data source
    self.expenseTableView.delegate = self;
    self.expenseTableView.dataSource = self;
    
    // Load the categories
    [self.fetchedResultsController performFetch:nil];
    
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE MMMM d, yyyy"];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expenseCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
    cell.selectedBackgroundView = selectedBackgroundView;
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:18.0f]];
    [cell.detailTextLabel setTextColor:[UIColor whiteColor]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:12.0f]];
    
    SVExpense *expense = [self.fetchedResultsController objectAtIndexPath:indexPath];
    SVCategory *category = [self getCategoryForGUID:expense.category];
    cell.textLabel.text = [NSString stringWithFormat:@"$%.02f on %@", expense.amount, category.name];
    
    NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:expense.date];
    cell.detailTextLabel.text = [dateFormatter stringFromDate:date];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
}

#pragma mark - Fetched Results Controller

- (NSFetchRequest *)categoryFetchRequest
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVExpense"];
    
    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"budget == %@", navController.currentBudget.guid];
    [request setPredicate:predicate];
    
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
    return request;
}

- (NSFetchedResultsController *) fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSFetchRequest *fetchRequest = [self categoryFetchRequest];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:coreDataStack.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (SVCategory *)getCategoryForGUID:(NSString *)GUID
{
    SVCategory *category = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"guid == %@", GUID];
    [request setPredicate:predicate];
    
    NSError *error;
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSArray *results = [coreDataStack.managedObjectContext executeFetchRequest:request error:&error];
    
    if (results != nil && results.count == 1) {
        category = results.firstObject;
    }
    
    return category;
}

@end
