//
//  MLWaterLevelView.m
//  BudgetFish
//
//  Created by Steve Viselli on 19/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVWaterLevelView.h"
#import "HexColors/UIColor+HexColors.h"

@interface SVWaterLevelView()

@property (nonatomic) CFTimeInterval firstTimestamp;
@property (nonatomic) CFTimeInterval displayLinkTimestamp;
@property (nonatomic) NSUInteger loopCount;
@property (nonatomic) CGFloat seconds;

@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic, strong) UIImageView *budgetFishView;

@end

@implementation SVWaterLevelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

/**
 * Thanks to http://www.teehanlax.com/blog/introduction-to-uimotioneffect/
 */
- (void)budgetFishParallax
{
    // Set vertical effect
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-10);
    verticalMotionEffect.maximumRelativeValue = @(10);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-10);
    horizontalMotionEffect.maximumRelativeValue = @(10);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    // Add both effects to the view
    [self.budgetFishView addMotionEffect:group];
}

- (void)startDisplayLinkForSeconds:(float)seconds
{
    self.firstTimestamp = 0;
    self.loopCount = 0;
    self.seconds = seconds;
    
    if (!_displayLink) {
        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(handleDisplayLink:)];
        [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
}

- (void)stopDisplayLink
{
    [self.displayLink invalidate];
    self.displayLink = nil;
}

- (void)handleDisplayLink:(CADisplayLink *)displayLink
{
    self.loopCount++;
    
    if (!self.firstTimestamp) {
        self.firstTimestamp = displayLink.timestamp;
    }
    
    self.displayLinkTimestamp = displayLink.timestamp;
    
    [self setNeedsDisplayInRect:self.bounds];
    
    NSTimeInterval elapsed = (displayLink.timestamp - self.firstTimestamp);
    
    if (elapsed >= self.seconds)
    {
        [self stopDisplayLink];
        self.displayLinkTimestamp = self.firstTimestamp + self.seconds;
        [self setNeedsDisplayInRect:self.bounds];
    }
}

/**
 * Thanks to http://stackoverflow.com/questions/14571148/ios-animate-transformation-from-a-line-to-a-bezier-curve
 */
- (UIBezierPath *)pathAtInterval:(NSTimeInterval) interval forRect:(CGRect)rect
{
    // Set curve width to screen width / number of curves
    CGFloat curveWidth = rect.size.width / NUMBER_OF_CURVES;
    // Add the width of one trough so that we have peaks on both ends of the curve
    curveWidth += curveWidth / (NUMBER_OF_CURVES + NUMBER_OF_CURVES - 1);
    
    CGFloat fractionOfSecond = interval - floor(interval);
    CGFloat yOffset = self.bounds.size.height / CURVE_AMPLITUDE * sin(fractionOfSecond * M_PI * 2.0) / (interval + 1);
    
    CGRect waterRect = CGRectMake(rect.origin.x, rect.origin.y + CURVE_HEIGHT / 2.0, rect.size.width, rect.size.height - CURVE_HEIGHT / 2.0);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:waterRect];
    [path moveToPoint:waterRect.origin];
    
    for (int i = 0; i < NUMBER_OF_CURVES; i++) {
        CGPoint startPoint = [path currentPoint];
        CGPoint endPoint = CGPointMake(startPoint.x + curveWidth, startPoint.y);
        CGPoint controlPoint1 = CGPointMake(startPoint.x + curveWidth / 3.0 * 1.2, waterRect.origin.y - CURVE_HEIGHT - yOffset);
        CGPoint controlPoint2 = CGPointMake(startPoint.x + curveWidth / 3.0 * 1.8, waterRect.origin.y + CURVE_HEIGHT + yOffset);
        [path addCurveToPoint:endPoint controlPoint1:controlPoint1 controlPoint2:controlPoint2];
    }
    
    return path;
}

- (void)drawRect:(CGRect)rect
{
    CGFloat locations[2] = { 0.1, 0.9 };
    UIColor *topColor = [UIColor colorWithHexString:@"0D58A6"];
    UIColor *bottomColor = [UIColor colorWithHexString:@"022140"];
    NSArray *colors = @[(__bridge id) topColor.CGColor, (__bridge id) bottomColor.CGColor];
    
    CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef myGradient = CGGradientCreateWithColors(myColorspace, (__bridge CFArrayRef) colors, locations);
    CGColorSpaceRelease(myColorspace), myColorspace = NULL;
    
    NSTimeInterval elapsed = (self.displayLinkTimestamp - self.firstTimestamp);
    UIBezierPath *path = [self pathAtInterval:elapsed forRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextAddPath(context, path.CGPath);
    CGContextClip(context);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextDrawLinearGradient(context, myGradient, startPoint, endPoint, 0);
    CGGradientRelease(myGradient), myGradient = NULL;
    
    CGContextRestoreGState(context);
}

- (void)drawWaterLevelForBudgetUsed:(float)budgetUsed animated:(BOOL)animated
{
    if (budgetUsed > 1.0f) {
        budgetUsed = 1.0f;
    }
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGRect mainBounds = mainScreen.bounds;
    float screenHeight = mainBounds.size.height;
    
    float waterLevelTopOffset = 65.0f; // Just under the nav bar
    float waterLevelBottomOffset = 00.0f; // Enough room so that the fish doesn't go off screen
    float availableScreen = (screenHeight - waterLevelTopOffset - waterLevelBottomOffset);
    float currentLevel = availableScreen * budgetUsed + waterLevelTopOffset;
    
    // Make the water a bit taller than the screen so that we don't overshoot the screen on animation
    CGRect waterLevelFrame = CGRectMake(0, currentLevel, mainBounds.size.width, mainBounds.size.height + 20);
    float distanceToTravel = abs(self.frame.origin.y - waterLevelFrame.origin.y);
    float animationDuration = pow(distanceToTravel, 0.25);
    
    if (animated) {
        // End the wave oscillation animation a bit before the water moving animation
        [self startDisplayLinkForSeconds:animationDuration - (animationDuration/8)];
        [self slideAnimateView:self
                       toFrame:waterLevelFrame
                      duration:animationDuration
                         delay:0.0
                    completion:^(BOOL finished){
                        if (finished) {
                            [self stopDisplayLink];
                        }
                    }];
    }
    else {
        [self setFrame:waterLevelFrame];
    }
    
    float fishDistanceFromRightEdge = 38.0f;
    float fishDistanceFromWaterSurface = 16.0f;
    float fishWidth = 24.0f;
    float fishHeight = 16.0f;
    CGRect budgetFishFrame = CGRectMake(CGRectGetMaxX(waterLevelFrame) - fishDistanceFromRightEdge, fishDistanceFromWaterSurface, fishWidth, fishHeight);
    
    if (self.budgetFishView == nil) {
        self.budgetFishView = [[UIImageView alloc] initWithFrame:budgetFishFrame];
        [self.budgetFishView setImage:[UIImage imageNamed:@"Budget_Fish"]];
        [self.budgetFishView setAlpha:0.7f];
        [self addSubview:self.budgetFishView];
        [self budgetFishParallax];
    }
    else if (animated) {
        [self slideAnimateView:self.budgetFishView toFrame:budgetFishFrame duration:animationDuration delay:0.05 completion:nil];
    } else {
        [self.budgetFishView setFrame:budgetFishFrame];
    }
}

- (void)slideAnimateView:(UIView *)view toFrame:(CGRect)frame duration:(float) duration delay:(float)delay completion:(void(^)(BOOL))completion
{
    [UIView animateWithDuration:duration
                          delay:delay
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.1
                        options:UIViewAnimationOptionCurveEaseIn|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [view setFrame:frame];
                     }
                     completion:completion];
}

- (void)fadeOutFish
{
    [UIView animateWithDuration:0.2f animations:^{
        [self.budgetFishView setAlpha:0.0f];
    }];
}

- (void)fadeInFish
{
    [UIView animateWithDuration:0.2f animations:^{
        [self.budgetFishView setAlpha:1.0f];
    }];
}

@end
