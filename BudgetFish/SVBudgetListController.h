//
//  SVBudgetListController.h
//  BudgetFish
//
//  Created by Steve Viselli on 6/11/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVBudgetListController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *budgetTableView;
@property (weak, nonatomic) IBOutlet UIView *bottomNavView;

@end
