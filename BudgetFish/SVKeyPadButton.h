//
//  MLKeyPadButton.h
//  BudgetFish
//
//  Created by Steve Viselli on 17/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVKeyPadButton : UIButton

- (id)initWithFrame:(CGRect)frame label:(NSString *)label;
- (id)initWithFrame:(CGRect)frame image:(UIImage *)image fillColor:(UIColor *)fillColor;

@end
