//
//  MLWaterLevelView.h
//  BudgetFish
//
//  Created by Steve Viselli on 19/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CURVE_HEIGHT 12.0f
#define NUMBER_OF_CURVES 3
#define CURVE_AMPLITUDE 80 // [1-100] 1 = very high amplitude, 100 = very low amplitude

@interface SVWaterLevelView : UIView

- (void)drawWaterLevelForBudgetUsed:(float)budgetUsed animated:(BOOL)animated;
- (void)startDisplayLinkForSeconds:(float)seconds;
- (void)stopDisplayLink;
- (void)fadeOutFish;
- (void)fadeInFish;

@end
