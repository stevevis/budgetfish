//
//  SVBudget.h
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SVSyncable.h"

NS_ENUM(int16_t, SVBudgetPeriod) {
    SVBudgetPeriodWeekly = 0,
    SVBudgetPeriodMonthly = 1
};

@interface SVBudget : SVSyncable

@property (nonatomic) int16_t period;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) double limit;
@property (nonatomic) Boolean is_default;

@end
