//
//  SVSyncable.h
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SVSyncable : NSManagedObject

@property (nonatomic, retain) NSString *guid;
@property (nonatomic) BOOL is_deleted;
@property (nonatomic) NSTimeInterval last_modified;
@property (nonatomic) int16_t sync_statuc;

@end
