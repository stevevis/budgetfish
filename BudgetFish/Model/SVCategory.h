//
//  SVCategory.h
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SVSyncable.h"


@interface SVCategory : SVSyncable

@property (nonatomic, retain) NSString *name;

@end
