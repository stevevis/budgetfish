//
//  SVExpense.h
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SVSyncable.h"


@interface SVExpense : SVSyncable

@property (nonatomic) double amount;
@property (nonatomic, retain) NSString *budget;
@property (nonatomic, retain) NSString *category;
@property (nonatomic) NSTimeInterval date;
@property (nonatomic, retain) NSString *location;

@end
