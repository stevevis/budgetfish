//
//  SVBudget.m
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVBudget.h"

@implementation SVBudget

@dynamic period;
@dynamic name;
@dynamic limit;
@dynamic is_default;

- (NSString *)description
{
    return [NSString stringWithFormat: @"Budget: Name=%@, Peroid=%d, Limit=%g, IsDefault=%d", self.name, self.period, self.limit, self.is_default];
}

@end
