//
//  SVExpense.m
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVExpense.h"


@implementation SVExpense

@dynamic amount;
@dynamic budget;
@dynamic category;
@dynamic date;
@dynamic location;

- (NSString *)description
{
    return [NSString stringWithFormat: @"Expense: Amount=%g, Date=%f, Location=%@", self.amount, self.date, self.location];
}

@end
