//
//  SVSyncable.m
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVSyncable.h"


@implementation SVSyncable

@dynamic guid;
@dynamic is_deleted;
@dynamic last_modified;
@dynamic sync_statuc;

@end
