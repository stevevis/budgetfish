//
//  SVCategory.m
//  BudgetFish
//
//  Created by Steve Viselli on 15/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVCategory.h"


@implementation SVCategory

@dynamic name;

- (NSString *)description
{
    return [NSString stringWithFormat: @"Category: Name=%@", self.name];
}

@end
