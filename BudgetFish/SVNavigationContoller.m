//
//  SVNavigationContoller.m
//  BudgetFish
//
//  Created by Steve Viselli on 21/09/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVNavigationContoller.h"
#import "SVExpenseInputController.h"
#import "SVWaterLevelView.h"
#import "HexColors/UIColor+HexColors.h"
#import "SVSlideNavigationAnimator.h"
#import "SVCoreDataStack.h"
#import "Model/SVExpense.h"

static NSCalendar *calendar = nil;

@interface SVNavigationContoller ()

@property (nonatomic, strong) SVSlideNavigationAnimator *slideAnimator;
@property (nonatomic, strong) SVWaterLevelView *waterLevelView;

@end

@implementation SVNavigationContoller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    
    self.slideAnimator = [SVSlideNavigationAnimator new];
    
    // Setup the background gradient
    CAGradientLayer *backgroundLayer = [SVNavigationContoller backgroundGradientLayer];
    backgroundLayer.frame = self.view.frame;
    [self.view.layer insertSublayer:backgroundLayer atIndex:0];
    
    // Create the water level view
    self.waterLevelView = [[SVWaterLevelView alloc] initWithFrame:CGRectZero];
    [self.view insertSubview:self.waterLevelView atIndex:1];
    [self.waterLevelView drawWaterLevelForBudgetUsed:1.0f animated:false];
    
    if (calendar == nil) {
        calendar = [NSCalendar autoupdatingCurrentCalendar];
    }
    
    [self updateBudget];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self updateWaterLevel];
}

- (void)updateBudget
{
    if (self.currentBudget == nil) {
        self.currentBudget = [self getDefaultBudget];
    }
    
    float spent = [self loadExpenseTotalForCurrentWeek];
    _budgetUsed = spent/self.currentBudget.limit;

    NSLog(@"Current budget is %@", self.currentBudget);
    NSLog(@"Amount spent this period is %f", spent);
}

- (void)updateWaterLevel
{
    [self.waterLevelView drawWaterLevelForBudgetUsed:_budgetUsed animated:true];
}

- (SVBudget *)getDefaultBudget
{
    SVBudget *budget = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVBudget"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_default == YES"];
    [request setPredicate:predicate];
    
    NSError *error;
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSArray *results = [coreDataStack.managedObjectContext executeFetchRequest:request error:&error];
    
    if (results != nil && results.count == 1) {
        budget = results.firstObject;
    }
    
    return budget;
}

- (float)loadExpenseTotalForCurrentWeek
{
    float total = 0.0;
    
    // Calculate start and end dates of the current week
    NSDate *now = [NSDate date];
    NSDate *startOfWeek;
    NSDate *endOfWeek;
    NSTimeInterval interval;
    [calendar rangeOfUnit:NSCalendarUnitWeekOfYear
           startDate:&startOfWeek
            interval:&interval
             forDate:now];
    endOfWeek = [startOfWeek dateByAddingTimeInterval:interval-1];
    
    NSLog(@"Searching for expenses between %@ and %@", startOfWeek, endOfWeek);
    
    // Create the core data fetch request for this week's expenses
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVExpense"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(date > %@) AND (date < %@)", startOfWeek, endOfWeek];
    [request setPredicate:predicate];
    
    NSError *error;
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSArray *expenses = [coreDataStack.managedObjectContext executeFetchRequest:request error:&error];
    
    // Iterate over expenses and add up total
    if (expenses != nil) {
        NSLog(@"Found %lu expenses", (unsigned long)expenses.count);
        for (SVExpense *expense in expenses) {
            NSLog(@"Expense: %@", expense);
            total += expense.amount;
        }
    }
    
    return total;
}

//- (void)setBudgetUsed:(float)budgetUsed
//{
//    _budgetUsed = budgetUsed;
//    [self.waterLevelView drawWaterLevelForBudgetUsed:budgetUsed animated:true];
//}

+ (CAGradientLayer *)backgroundGradientLayer
{
    UIColor *topColor = [UIColor colorWithHexString:@"0A64A4"];
    UIColor *bottomColor = [UIColor colorWithHexString:@"3D80B0"];
    
    NSArray *gradientColors = [NSArray arrayWithObjects:(id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    NSArray *gradientLocations = [NSArray arrayWithObjects:[NSNumber numberWithInt:0.0],[NSNumber numberWithInt:1.0], nil];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = gradientColors;
    gradientLayer.locations = gradientLocations;
    
    return gradientLayer;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC
{
    if (operation == UINavigationControllerOperationPush) {
        [self.slideAnimator setSlideDirection:SlideLeft];
        return self.slideAnimator;
    }
    else if (operation == UINavigationControllerOperationPop) {
        [self.slideAnimator setSlideDirection:SlideRight];
        return self.slideAnimator;
    }
    
    return nil;
}

/**
 * Deletes all expenses in core data, only for testing!
 */
- (void)deleteAllExpenses
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVExpense"];
    [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error;
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSArray *expenses = [coreDataStack.managedObjectContext executeFetchRequest:request error:&error];

    if (expenses != nil) {
        for (SVExpense *expense in expenses) {
            [coreDataStack.managedObjectContext deleteObject:expense];
        }
    }

    NSError *saveError;
    [coreDataStack.managedObjectContext save:&saveError];
    
    [self updateBudget];
    [self updateWaterLevel];
}

- (void)fadeOutFish
{
    [self.waterLevelView fadeOutFish];
}

- (void)fadeInFish
{
    [self.waterLevelView fadeInFish];
}

@end
