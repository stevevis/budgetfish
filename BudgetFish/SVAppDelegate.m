//
//  MLAppDelegate.m
//  BudgetFish
//
//  Created by Steve Viselli on 16/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVAppDelegate.h"
#import "SVCoreDataStack.h"
#import "Model/SVCategory.h"
#import "Model/SVBudget.h"
#import "HexColors/UIColor+HexColors.h"

@implementation SVAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //UIColor *navBackground = [UIColor colorWithWhite:1.0 alpha:0.1];
    UIColor *navBackground = [UIColor colorWithHexString:@"2776AE"];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 20)];
    view.backgroundColor = navBackground;
    [self.window.rootViewController.view insertSubview:view atIndex:100];
    
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    navigationBarAppearance.backgroundColor = navBackground;
    [navigationBarAppearance setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    navigationBarAppearance.shadowImage = [UIImage new];
    
    // Shake to delete all expenses for debug
    application.applicationSupportsShakeToEdit = YES;
    
    [self seedCategories];
    [self seedBudgets];
    
    return YES;
}

- (void)seedCategories
{
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVCategory"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    
    NSError *error;
    NSArray *array = [coreDataStack.managedObjectContext executeFetchRequest:request error:&error];
    
    if (array != nil && array.count == 0) {
        // Seed the category entity with some default categories
        NSArray *defaultCategories = @[@"Bills", @"Rent", @"Food/Drink", @"Entertainment", @"Clothes", @"Gift"];
        
        for (NSString *name in defaultCategories) {
            SVCategory *category = [NSEntityDescription insertNewObjectForEntityForName:@"SVCategory" inManagedObjectContext:coreDataStack.managedObjectContext];
            category.guid = [[NSUUID UUID] UUIDString];
            category.sync_statuc = 1;
            category.last_modified = [[NSDate date] timeIntervalSince1970];
            category.is_deleted = 0;
            category.name = name;
            NSLog(@"Saving category: %@", category);
        }
        
        [coreDataStack saveContext];
    } else {
        for (SVCategory *category in array) {
            NSLog(@"Existing category: %@", category);
        }
    }
}

- (void)seedBudgets
{
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVBudget"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    
    NSError *error;
    NSArray *array = [coreDataStack.managedObjectContext executeFetchRequest:request error:&error];
    
    if (array != nil && array.count == 0) {
        // Seed the budget entity with a default budget
        NSArray *defaultBudgets = @[@{@"name": @"My Budget", @"period": [NSNumber numberWithInt: SVBudgetPeriodMonthly], @"limit": @1000, @"is_default": @YES}];
        
        for (NSDictionary *defaultBudget in defaultBudgets) {
            SVBudget *budget = [NSEntityDescription insertNewObjectForEntityForName:@"SVBudget" inManagedObjectContext:coreDataStack.managedObjectContext];
            budget.guid = [[NSUUID UUID] UUIDString];
            budget.sync_statuc = 1;
            budget.last_modified = [[NSDate date] timeIntervalSince1970];
            budget.is_deleted = 0;
            budget.name = defaultBudget[@"name"];
            budget.period = [(NSNumber *)defaultBudget[@"period"] integerValue];
            budget.limit = [defaultBudget[@"limit"] doubleValue];
            budget.is_default = [defaultBudget[@"is_default"] boolValue];
            NSLog(@"Saving budget: %@", budget);
        }
        
        [coreDataStack saveContext];
    } else {
        for (SVBudget *budget in array) {
            NSLog(@"Existing budget: %@", budget);
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    [coreDataStack saveContext];
}

@end
