//
//  SVSelectCategoryController.h
//  BudgetFish
//
//  Created by Steve Viselli on 21/09/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVExpenseCreatedDelegate.h"

@interface SVSelectCategoryController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) id <SVExpenseCreatedDelegate> expenseCreatedDelegate;

@property (nonatomic, strong) NSNumber *expenseAmount;
@property (nonatomic, strong) NSString *expenseCategory;

@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property (weak, nonatomic) IBOutlet UIView *bottomNavView;
@property (weak, nonatomic) IBOutlet UIButton *createButton;

- (IBAction)cancel:(id)sender;
- (IBAction)spend:(id)sender;

@end
