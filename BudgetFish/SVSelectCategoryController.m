//
//  SVSelectCategoryController.m
//  BudgetFish
//
//  Created by Steve Viselli on 21/09/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVSelectCategoryController.h"
#import "SVCoreDataStack.h"
#import "Model/SVCategory.h"
#import "Model/SVExpense.h"
#import "SVExpenseInputController.h"
#import "SVNavigationContoller.h"

@interface SVSelectCategoryController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation SVSelectCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Make sure everything is clear
    UIColor *clearColor = [UIColor clearColor];
    [self.view setBackgroundColor:clearColor];
    [self.bottomNavView setBackgroundColor:clearColor];
    
    // Make the table view clear
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectZero];
    [clearView setBackgroundColor:clearColor];
    [self.categoryTableView setBackgroundView:clearView];
    [self.categoryTableView setBackgroundColor:clearColor];
    
    // Don't need a navigation back button
    self.navigationItem.hidesBackButton = true;
    
    // Set the table view delegate and data source
    self.categoryTableView.delegate = self;
    self.categoryTableView.dataSource = self;
    
    // Load the categories
    [self.fetchedResultsController performFetch:nil];
    
    // The create button should be disabled until a category is selected
    [self.createButton setEnabled:false];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
    cell.selectedBackgroundView = selectedBackgroundView;
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:20.0f]];
    
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_empty"]];
    
    SVCategory *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = category.name;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SVCategory *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (self.expenseCategory == category.guid) {
        [self.createButton setEnabled:false];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_empty"]];
        self.expenseCategory = nil;
    } else {
        [self.createButton setEnabled:true];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_tick"]];
        self.expenseCategory = category.guid;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_empty"]];
}

#pragma mark - Fetched Results Controller

- (NSFetchRequest *)categoryFetchRequest
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVCategory"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    return request;
}

- (NSFetchedResultsController *) fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSFetchRequest *fetchRequest = [self categoryFetchRequest];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:coreDataStack.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)spend:(id)sender
{
    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    
    // Create the new expense
    SVExpense *expense = [NSEntityDescription insertNewObjectForEntityForName:@"SVExpense" inManagedObjectContext:coreDataStack.managedObjectContext];
    expense.guid = [[NSUUID UUID] UUIDString];
    expense.sync_statuc = 1;
    expense.last_modified = [[NSDate date] timeIntervalSince1970];
    expense.is_deleted = 0;
    expense.amount = [self.expenseAmount doubleValue];
    expense.category = self.expenseCategory;
    expense.budget = navController.currentBudget.guid;
    expense.date = [[NSDate date] timeIntervalSinceReferenceDate];
        
    // Save the expense into the context
    [coreDataStack saveContext];
    
    [self.expenseCreatedDelegate expenseCreated:expense];
    [self.navigationController popViewControllerAnimated:true];
}

@end
