//
//  MLExpenseInputController.m
//  BudgetFish
//
//  Created by Steve Viselli on 16/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVExpenseInputController.h"
#import "SVNavigationContoller.h"
#import "SVBudgetListController.h"
#import "SVKeyPadButton.h"
#import "SVWaterLevelView.h"
#import "SVSelectCategoryController.h"
#import "HexColors/UIColor+HexColors.h"
#import "Model/SVExpense.h"
#import "SVExpenseCreatedDelegate.h"

@interface SVExpenseInputController () <SVExpenseCreatedDelegate>

@property (nonatomic, strong) SVKeyPadButton *oneButton;
@property (nonatomic, strong) SVKeyPadButton *twoButton;
@property (nonatomic, strong) SVKeyPadButton *threeButton;
@property (nonatomic, strong) SVKeyPadButton *fourButton;
@property (nonatomic, strong) SVKeyPadButton *fiveButton;
@property (nonatomic, strong) SVKeyPadButton *sixButton;
@property (nonatomic, strong) SVKeyPadButton *sevenButton;
@property (nonatomic, strong) SVKeyPadButton *eightButton;
@property (nonatomic, strong) SVKeyPadButton *nineButton;
@property (nonatomic, strong) SVKeyPadButton *zeroButton;
@property (nonatomic, strong) SVKeyPadButton *decimalButton;
@property (nonatomic, strong) SVKeyPadButton *deleteButton;
@property (nonatomic, strong) UIView *expenseMask;

@end

static NSString *empty = @"0";

@implementation SVExpenseInputController

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    // Don't need a navigation back button
    self.navigationItem.hidesBackButton = true;
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGRect mainBounds = mainScreen.bounds;
    // Screen height minus height of nav bar and bottom buttons
    float screenHeight = mainBounds.size.height - 120;
    
    float buttonSize = screenHeight / 7.2f;
    float buttonSpacing = buttonSize / 4.0f;
    float buttonCenterDifference = buttonSize + buttonSpacing;
    
    float centerOffsetY = 5; // Manually adjust Y offset if necessary
    CGPoint center = CGPointMake(self.view.center.x, self.view.center.y - centerOffsetY);
    [self createExpenseFieldWithHeight:buttonSize centerX:center.x centerY:center.y-buttonCenterDifference*2];
    
    self.oneButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"1"];
    self.oneButton.center = CGPointMake(center.x-buttonCenterDifference, center.y-buttonCenterDifference);
    [self.oneButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.oneButton];
    
    self.twoButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"2"];
    self.twoButton.center = CGPointMake(center.x, center.y-buttonCenterDifference);
    [self.twoButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.twoButton];
    
    self.threeButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"3"];
    self.threeButton.center = CGPointMake(center.x+buttonCenterDifference, center.y-buttonCenterDifference);
    [self.threeButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.threeButton];
    
    self.fourButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"4"];
    self.fourButton.center = CGPointMake(center.x-buttonCenterDifference, center.y);
    [self.fourButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.fourButton];
    
    self.fiveButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"5"];
    self.fiveButton.center = center;
    [self.fiveButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.fiveButton];
    
    self.sixButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"6"];
    self.sixButton.center = CGPointMake(center.x+buttonCenterDifference, center.y);
    [self.sixButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sixButton];
    
    self.sevenButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"7"];
    self.sevenButton.center = CGPointMake(center.x-buttonCenterDifference, center.y+buttonCenterDifference);
    [self.sevenButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sevenButton];
    
    self.eightButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"8"];
    self.eightButton.center = CGPointMake(center.x, center.y+buttonCenterDifference);
    [self.eightButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.eightButton];
    
    self.nineButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"9"];
    self.nineButton.center = CGPointMake(center.x+buttonCenterDifference, center.y+buttonCenterDifference);
    [self.nineButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nineButton];
    
    self.zeroButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"0"];
    self.zeroButton.center = CGPointMake(center.x-buttonCenterDifference, center.y+buttonCenterDifference*2);
    [self.zeroButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.zeroButton setEnabled:false];
    [self.view addSubview:self.zeroButton];
    
    self.decimalButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) label:@"."];
    self.decimalButton.center = CGPointMake(center.x, center.y+buttonCenterDifference*2);
    [self.decimalButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.decimalButton];
    
    UIImage *deleteImage = [UIImage imageNamed:@"Delete_Icon"];
    UIColor *deleteColor = [UIColor colorWithHexString:@"C2515F"];
    self.deleteButton = [[SVKeyPadButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize) image:deleteImage fillColor:deleteColor];
    self.deleteButton.center = CGPointMake(center.x+buttonCenterDifference, center.y+buttonCenterDifference*2);
    [self.deleteButton addTarget:self action:@selector(keypadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteButton setEnabled:false];
    [self.view addSubview:self.deleteButton];
    
    // Initially disable the select category button
    [self.categoryButton setEnabled:false];
    
    // Add the budgets list view controller to the navigation stack
    NSArray *VCArray = [self.navigationController viewControllers];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    SVBudgetListController *budgetListController = [storyboard instantiateViewControllerWithIdentifier:@"BudgetListController"];
    NSMutableArray *newVCArray = [NSMutableArray arrayWithObject:budgetListController];
    [newVCArray addObjectsFromArray:VCArray];
    [self.navigationController setViewControllers:newVCArray animated:YES];
    [budgetListController setTitle:@"Budgets"];
    [budgetListController.navigationItem setTitle:@"Budgets"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
    [navController fadeInFish];
    
    NSMutableString *title = [[NSMutableString alloc] initWithString:navController.currentBudget.name];
    float budgetLeft = (1.0f - [navController budgetUsed]) * 100;
    int budgetLeftPercent = (int)roundf(budgetLeft);
    [title appendString:[NSString stringWithFormat:@" %d%%", budgetLeftPercent]];
    [self setTitle:title];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
    [navController fadeOutFish];
    [super viewWillDisappear:animated];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
        [navController deleteAllExpenses];
    }
}

- (void)createExpenseFieldWithHeight:(float)height centerX:(float)x centerY:(float)y
{
    CGRect expenseFieldFrame = CGRectMake(0, 0, height * 4, height);
    self.expenseField = [[UITextField alloc] initWithFrame:expenseFieldFrame];
    self.expenseField.center = CGPointMake(x, y);
    
    // Setup the expense field style
    [self.expenseField setBorderStyle:UITextBorderStyleNone];
    [self.expenseField setTextAlignment:NSTextAlignmentCenter];
    [self.expenseField setTextColor:[UIColor whiteColor]];
    [self.expenseField setTintColor:[UIColor clearColor]];
    
    // Set the height and font size of the expense display field
    float expenseFontSize = height;
    UIFontDescriptor *fontDescriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorFamilyAttribute: @"Helvetica Neue",  UIFontDescriptorTraitsAttribute: @{UIFontWeightTrait: @-0.8f}}];
    UIFont *inputFont = [UIFont fontWithDescriptor:fontDescriptor size:expenseFontSize];
    
    self.expenseField.text = @"0";
    self.expenseField.font = inputFont;
    self.expenseField.adjustsFontSizeToFitWidth = YES;
    
    // Add the expense field to the view
    [self.view addSubview:self.expenseField];
    
    // Cover the expense field with a UIView to prevent unwanted input or selection
    self.expenseMask = [[UIView alloc] initWithFrame:self.expenseField.frame];
    [self.view addSubview:self.expenseMask];
    
    // Set the input view to an empty view so that we don't show the keyboard
    self.expenseField.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.expenseField becomeFirstResponder];
}

/**
 * Reset the expense input view to the default 0 state.
 */
- (void)resetExpenseInput
{
    self.expenseField.text = empty;
    [self setDefaultKeypadState];
}

/**
 * Get the current expnese as a number value.
 */
- (NSNumber *)getCurrentExpense
{
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * currentExpense = [numberFormatter numberFromString:self.expenseField.text];
    return currentExpense;
}

/**
 * Responds to keypad buttons being pressed, updates the expense field.
 */
- (void)keypadButtonPressed:(id)sender
{
    SVKeyPadButton *button = (SVKeyPadButton *)sender;
    NSString *buttonLabel = button.titleLabel.text;
    
    NSString *currentTotal = self.expenseField.text;
    NSInteger currentLength = [currentTotal length];
    
    NSString *decimal = self.decimalButton.titleLabel.text;
    NSInteger decimalLocation = [currentTotal rangeOfString:decimal options:NSLiteralSearch].location;
    
//    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
//    if ([buttonLabel isEqualToString:decimal]) {
//        [navController setBudgetUsed:1.0f];
//    }
//    else {
//        NSInteger budgetUsed = [buttonLabel integerValue];
//        [navController setBudgetUsed:budgetUsed/10.0f];
//    }
    
    NSString *newTotal;
    
    if (buttonLabel == DELETE_LABEL) {
        if (currentLength > 0 && ![currentTotal isEqualToString:empty]) {
            newTotal = [currentTotal substringToIndex:currentLength - 1];
            self.expenseField.text = newTotal;
            
            // Enable the decimal button if we just deleted the decimal
            if ([[currentTotal substringWithRange:NSMakeRange(currentLength - 1, 1)] isEqualToString:decimal]
                    || (decimalLocation == NSNotFound && newTotal.length < MAX_EXPENSE_LENGTH - 2)) {
                [self.decimalButton setEnabled:true];
            }
        }
    }
    else if ([buttonLabel isEqualToString:decimal]) {
        if (decimalLocation == NSNotFound && currentLength < MAX_EXPENSE_LENGTH - 2) {
            // Append the decimal to the total
            newTotal = [NSString stringWithFormat:@"%@%@", currentTotal, buttonLabel];
            self.expenseField.text = newTotal;
            
            // Disable the decimal button
            [self.decimalButton setEnabled:false];
        }
    }
    else if (currentLength < MAX_EXPENSE_LENGTH) {
        // If the current total is 0, remove it so it is replaced with the new value
        if ([self.expenseField.text isEqualToString:empty]) {
            self.expenseField.text = @"";
        }
        
        // If there's no decimal yet, we can go to max limit
        if (decimalLocation == NSNotFound) {
            newTotal = [NSString stringWithFormat:@"%@%@", self.expenseField.text, buttonLabel];
            self.expenseField.text = newTotal;
            
            // If there isn't enough space for two more characters, disable the decimal button
            if ([newTotal length] >= MAX_EXPENSE_LENGTH - 2) {
                [self.decimalButton setEnabled:false];
            }
        }
        // If there is a decimal, only allow two characters after it
        else if (currentLength - decimalLocation < 3) {
            newTotal = [NSString stringWithFormat:@"%@%@", self.expenseField.text, buttonLabel];
            self.expenseField.text = newTotal;
        }
    }
    
    NSInteger newDecimalLocation = [newTotal rangeOfString:decimal options:NSLiteralSearch].location;
    
    // Disable all number buttons if max length is reached or two decimal places have been entered
    if (newTotal.length >= MAX_EXPENSE_LENGTH
            || (newDecimalLocation != NSNotFound && newTotal.length - newDecimalLocation > 2)) {
        [self setAllNumbersEnabled:false];
    } else {
        [self setAllNumbersEnabled:true];
    }
    
    
    if ([newTotal isEqualToString:@"0"] || newTotal.length == 0) {
        [self resetExpenseInput];
    } else {
        [self.deleteButton setEnabled:true];
    }
    
    // If the current expense is greater than 0, enable the category button
    if ([[self getCurrentExpense] floatValue] > 0) {
        [self.categoryButton setEnabled:true];
    } else {
        [self.categoryButton setEnabled:false];
    }
}

/**
 * Toggle the enabled state of all buttons.
 */
- (void)setAllNumbersEnabled:(Boolean)enabled {
    [self.oneButton setEnabled:enabled];
    [self.twoButton setEnabled:enabled];
    [self.threeButton setEnabled:enabled];
    [self.fourButton setEnabled:enabled];
    [self.fiveButton setEnabled:enabled];
    [self.sixButton setEnabled:enabled];
    [self.sevenButton setEnabled:enabled];
    [self.eightButton setEnabled:enabled];
    [self.nineButton setEnabled:enabled];
    [self.zeroButton setEnabled:enabled];
}

/**
 * Toggle the enabled state of all buttons.
 */
- (void)setDefaultKeypadState {
    [self.oneButton setEnabled:true];
    [self.twoButton setEnabled:true];
    [self.threeButton setEnabled:true];
    [self.fourButton setEnabled:true];
    [self.fiveButton setEnabled:true];
    [self.sixButton setEnabled:true];
    [self.sevenButton setEnabled:true];
    [self.eightButton setEnabled:true];
    [self.nineButton setEnabled:true];
    [self.zeroButton setEnabled:false];
    [self.decimalButton setEnabled:true];
    [self.deleteButton setEnabled:false];
    [self.categoryButton setEnabled:false];
}

#pragma mark - Expense Created Delegate Protocol

-(void)expenseCreated:(SVExpense *)expense
{
    [self resetExpenseInput];
    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
    [navController updateBudget];
    [navController updateWaterLevel];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Send the expense value to the category controller
    if ([segue.identifier isEqualToString:@"category"]) {
        SVSelectCategoryController *categoryController = segue.destinationViewController;
        [categoryController setExpenseCreatedDelegate:self];
        [categoryController setExpenseAmount:[self getCurrentExpense]];
    }
}

- (IBAction)budgets:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

@end
