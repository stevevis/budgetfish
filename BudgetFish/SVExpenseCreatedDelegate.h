//
//  SVExpenseCreatedDelegate.h
//  BudgetFish
//
//  Created by Steve Viselli on 28/10/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model/SVExpense.h"

@protocol SVExpenseCreatedDelegate <NSObject>

-(void)expenseCreated:(SVExpense *)expense;

@end
