//
//  MLExpenseInputController.h
//  BudgetFish
//
//  Created by Steve Viselli on 16/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAX_EXPENSE_LENGTH 9
#define DELETE_LABEL nil

@interface SVExpenseInputController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *budgetsButton;
@property (strong, nonatomic) IBOutlet UITextField *expenseField;

- (IBAction)budgets:(id)sender;

@end
