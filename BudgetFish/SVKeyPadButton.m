//
//  MLKeyPadButton.m
//  BudgetFish
//
//  Created by Steve Viselli on 17/08/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVKeyPadButton.h"
#import "HexColors/UIColor+HexColors.h"

@implementation SVKeyPadButton

UIImage *normalImage;
UIImage *highlightedImage;

- (id)initWithFrame:(CGRect)frame label:(NSString *)label
{
    self = [super initWithFrame:frame];
    if (self) {
        // Set the title to the number passed to the constructor
        [self setTitle:[NSString stringWithFormat:@"%@", label] forState:UIControlStateNormal];
        
        // Set the title color
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        // Set the title font
        float fontSize = frame.size.width / 1.5;
        UIFontDescriptor *fontDescriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorFamilyAttribute: @"Helvetica Neue", UIFontDescriptorTraitsAttribute: @{UIFontWeightTrait: @-0.8f}}];
        UIFont *labelFont = [UIFont fontWithDescriptor:fontDescriptor size:fontSize];
        self.titleLabel.font = labelFont;
        
        // Set the normal state image
        normalImage = [self drawButtonImageWithStrokeColor:[UIColor whiteColor] strokeWidth:0.6f fillColor:[UIColor whiteColor] fillAlpha:0.0f];
        [self setBackgroundImage:normalImage forState:UIControlStateNormal];
        
        // Set the highlight state image
        UIColor *highlightFill = [UIColor colorWithHexString:@"88AFD7"];
        highlightedImage = [self drawButtonImageWithStrokeColor:[UIColor whiteColor] strokeWidth:0.6f fillColor:highlightFill fillAlpha:0.3f];
        [self setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame image:(UIImage *)image fillColor:(UIColor *)fillColor;
{
    self = [super initWithFrame:frame];
    if (self) {
        // Set the button image
        [self setImage:image forState:UIControlStateNormal];
        self.adjustsImageWhenHighlighted = false;
        
        // Set the normal state image
        normalImage = [self drawButtonImageWithStrokeColor:fillColor strokeWidth:0.6f fillColor:fillColor fillAlpha:0.8f];
        [self setBackgroundImage:normalImage forState:UIControlStateNormal];
        
        // Set the highlight state image
        highlightedImage = [self drawButtonImageWithStrokeColor:fillColor strokeWidth:0.6f fillColor:fillColor fillAlpha:1.0f];
        [self setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    }
    return self;
}

- (UIImage *)drawButtonImageWithStrokeColor:(UIColor *)strokeColor strokeWidth:(float)strokeWidth fillColor:(UIColor* )fillColor fillAlpha:(float)fillAlpha
{
    UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0);
    
    UIBezierPath *aPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(1, 1, self.frame.size.width-2, self.frame.size.height-2)];
    
    // Set the render colors.
    [strokeColor setStroke];
    [fillColor setFill];
    
    // Adjust the drawing options as needed.
    aPath.lineWidth = strokeWidth + 0.4;
    
    [aPath strokeWithBlendMode:kCGBlendModeScreen alpha:0.6];
    [aPath fillWithBlendMode:kCGBlendModeScreen alpha:fillAlpha];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

@end
