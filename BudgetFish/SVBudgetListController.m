//
//  SVBudgetListController.m
//  BudgetFish
//
//  Created by Steve Viselli on 6/11/2014.
//  Copyright (c) 2014 Steve Viselli. All rights reserved.
//

#import "SVBudgetListController.h"
#import "SVExpenseInputController.h"
#import "SVCoreDataStack.h"
#import "Model/SVBudget.h"
#import "SVNavigationContoller.h"

@interface SVBudgetListController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation SVBudgetListController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Make sure everything is clear
    UIColor *clearColor = [UIColor clearColor];
    [self.view setBackgroundColor:clearColor];
    [self.bottomNavView setBackgroundColor:clearColor];
    
    // Make the table view clear
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectZero];
    [clearView setBackgroundColor:clearColor];
    [self.budgetTableView setBackgroundView:clearView];
    [self.budgetTableView setBackgroundColor:clearColor];
    
    // Don't need a navigation back button
    self.navigationItem.hidesBackButton = true;
    
    // Set the table view delegate and data source
    self.budgetTableView.delegate = self;
    self.budgetTableView.dataSource = self;
    
    // Load the categories
    [self.fetchedResultsController performFetch:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"budgetCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
    cell.selectedBackgroundView = selectedBackgroundView;
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:18.0f]];
    [cell.detailTextLabel setTextColor:[UIColor whiteColor]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:12.0f]];
    
    SVBudget *budget = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = budget.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"$%.02f", budget.limit];
    
    SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
    if ([navController.currentBudget isEqual:budget]) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_tick"]];
    } else {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_empty"]];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.budgetTableView.indexPathForSelectedRow != indexPath) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_tick"]];
    
        SVBudget *budget = [self.fetchedResultsController objectAtIndexPath:indexPath];
        SVNavigationContoller *navController = (SVNavigationContoller *)self.navigationController;
        navController.currentBudget = budget;
        [navController updateBudget];
        [navController updateWaterLevel];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle_empty"]];
}

#pragma mark - Fetched Results Controller

- (NSFetchRequest *)categoryFetchRequest
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SVBudget"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    return request;
}

- (NSFetchedResultsController *) fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    SVCoreDataStack *coreDataStack = [SVCoreDataStack defaultStack];
    NSFetchRequest *fetchRequest = [self categoryFetchRequest];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:coreDataStack.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

@end
